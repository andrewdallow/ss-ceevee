## CeeVee Template Adaption

Website template CeeVee developed by [styleshout](https://www.styleshout.com/free-templates/ceevee/) and adapted to the SilverStripe 3 Content Managment System (CMS) ([http://silverstripe.org](http://silverstripe.org)).

Can be used as a resume website showcasing ones skills and portfolio. The webiste a clean, modern, fully responsive website template.

## License ##
	Copyright 2016 Andrew Dallow

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
